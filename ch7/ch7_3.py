# think python chapter 7, exercise 3

# pi estimator using Ramanujan's formula

import math


def factorial(n):
    """ return n! - I assume there's a python math function for this
    already, just practicing writing more code

    >>> factorial(0)
    1
    >>> factorial(1)
    1
    >>> factorial(4)
    24
    >>> factorial(10)
    3628800
    """

    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)


def estimate_pi():
    """estimate pi using Ramanujan's formula"""

    k = 0
    k_accumulator = 0

    while True:
        fourk_fact = factorial(4 * k)
        top = fourk_fact * (1103 + (26390 * k))
        k_fact = factorial(k)
        bottom = math.pow(k_fact, 4) * (math.pow(396, 4 * k))
        term = top / bottom
        if abs(term) < 1e-15:
            break
        else:
            k_accumulator += term
        k += 1

        factor = ((2 * math.sqrt(2)) / 9801) * k_accumulator

    return 1 / factor


if __name__ == "__main__":
    print()
    my_estimate = estimate_pi()
    print("my estimate:", my_estimate)
    print("math.pi:", math.pi)
    print("difference:", abs(math.pi - my_estimate))
