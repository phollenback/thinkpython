# think python chapter 7, exercise 1

# newton's method of square root approximation

from tabulate import tabulate


def mysqrt(a):
    """ use newton's method to calculate square root"""

    # starting approximation for x
    x = a / 2
    y = 0

    # how close is close enough?
    epsilon = 0.0000000001

    while True:
        y = (x + a / x) / 2
        if abs(x - y) < epsilon:
            break
        x = y

    return x


def test_square_root(x):
    """test using newton's method for various values
    tests the first x square roots, starting with 1 and including x
    """

    results = []

    for i in range(1, x + 1):
        diff = abs(mysqrt(i) - math.sqrt(i))
        results.append([i, mysqrt(i), math.sqrt(i), diff])

    print(
        tabulate(
            results,
            headers=["a", "mysqrt(a)", "math.sqrt(a)", "diff"],
            colalign=("left"),
        )
    )


if __name__ == "__main__":
    print()
    test_square_root(20)
