# think python chapter 7, exercise 2

# write a function using eval to evaluate user input


def eval_loop():

    my_input = ""

    print('enter "done" to quit')
    while True:
        my_input = input("evaluate what? ")
        if my_input == "done":
            break
        print(eval(my_input))

    print("bye!")


if __name__ == "__main__":
    print()
    eval_loop()
