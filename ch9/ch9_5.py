# think python chapter 9, exercise 5

# A) given a word and a string of required letters, return True if the string
# uses all the required letters at least once

# B) Determine how many words in the wordlist use all of aeiou

# C) Determine how many words in the wordlist use all of aeiouy

# uses http://thinkpython2.com/code/words.txt


def uses_all(word, check_letters):
    """ given a word and a list of letters in check_letter, return
    True if the word uses all letters in check_letters at least once.
    """

    for c in check_letters:
        if c not in word:
            return False

    return True


def read_words():
    """ read the word list in to memory """
    f = open("./words.txt", "r+")
    words = [line.rstrip() for line in f.readlines()]
    f.close()
    return words


def check_word(word, required_chars):
    """ given a word and required letters, check if word
    contains all characters. """

    if uses_all(word, required_chars):
        result = "uses all characters in"
    else:
        result = "does NOT use all characters in"

    print(word, result, required_chars)


def check_words(wordlist, required_chars):
    """ given a wordlist and required letters, return count
     of how many words use all the required letters """

    counter = 0

    for word in wordlist:
        if uses_all(word, required_chars):
            counter += 1

    return counter


if __name__ == "__main__":
    print()

    my_words = read_words()

    print("Part A - write uses_all function:")
    check_word("words", "orw")
    check_word("elephant", "aples")
    check_word("tigon", "notig")

    print()
    print("Part B - how many words use aeiou?")
    print(check_words(my_words, "aeiou"))

    print()
    print("Part C - how many words use aeiouy?")
    print(check_words(my_words, "aeiouy"))
