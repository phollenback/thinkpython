# think python chapter 9, exercise 9

# another car talk puzzler

# my age has been the reverse of my mother's age 6 times.  If we live
# long enough it will happen again two more times.  How old am I now?

# example: If I was 37, my mother would be 73.

# defaultdict is just more convenient for doing counter hashes.
from collections import defaultdict


def gen_reversed_ages():
    """generate all reversed pairs of numbers between 1 and 99. """

    ages = []

    for i in range(1, 99):
        entry = str(i).zfill(2)
        rev = entry[::-1].zfill(2)
        ages.append([entry, rev])
    return ages


def find_reasonable_ages(ages):
    """ find sensible age pairs - where child and mother age distance is
    in a biologically reasonable range """

    valid_ages = []
    for (n, m) in ages:
        age_diff = int(m) - int(n)
        if 18 < age_diff < 60:
            valid_ages.append([int(m), int(n)])

    valid_ages.sort(key=lambda x: x[0])

    return valid_ages


def find_most_common_distance(ages):
    """the list of valid age pairs have to each have the same distance
    between them, ie son can't be 20 years younger than mother in one
    pair and 18 years younger in next.  This function returns the most
    common distance between all palindromic pairs.
    """

    diffs = defaultdict(int)

    for (n, m) in ages:
        diffs[n - m] += 1

    most_common = max(diffs, key=diffs.get)

    return most_common


def find_most_common_pairs(ages, most_common_distance):
    """ now that we know the most common distance between valid pairs,
    get just the list of entries which match that """

    matches = []
    for (n, m) in ages:
        if int(n) - int(m) == most_common_distance:
            matches.append([n, m])

    return matches


if __name__ == "__main__":
    print()

    ages = gen_reversed_ages()
    valid_ages = find_reasonable_ages(ages)
    most_common_distance = find_most_common_distance(valid_ages)
    matches = find_most_common_pairs(ages, most_common_distance)

    print("My mother and I have been palindromic ages 6 times.")
    print("What are our current ages?")
    print("I am", matches[5][1], "and my mother is", matches[5][0])
