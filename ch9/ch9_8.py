# think python chapter 9, exercise 8

# solve car talk palindrome odometer puzzle:
# odometer shows 6 digits in whole miles only
# beginning: last 4 digits palindrome
# add 1 mile, last 5 digits palindrome
# add 1 mile, all 6 digits palindrome
# what was the original odometer reading?


def gen_odo_palindromes():
    """ generate all 6 digit odometer readings which are palindromes """

    odo_entries = []

    for i in range(0, 999999):
        # manipulate as left-zero-padded strings for ease of use
        entry = str(i).zfill(6)
        # our trusty palindrome checker 1-liner
        if entry == entry[::-1]:
            odo_entries.append(entry)

    return odo_entries


def winnow_palindromes(odo_entries, width):
    """ find all <width>-digit palindromes in a 6-digit odometer
    these are numbers, so width is from the right and numbers are left-padded
    with zeroes """

    my_odo_entries = []

    # max width is 6 of course, so we can use that to calculate the string
    # offset.
    offset = 6 - width

    for i in odo_entries:
        minus_one = int(i) - 1
        # 000000 - 1 is special case since it rolls over backwards
        if minus_one == -1:
            minus_one = 999999
        # now left pad with zeroes and convert to strings
        entry = str(minus_one).zfill(6)
        # chop out the right <width> digits to palindrome test
        front = entry[offset:]
        back = front[::-1]
        if front == back:
            # put the original odo reading on the valid list
            my_odo_entries.append(entry)

    return my_odo_entries


if __name__ == "__main__":
    print()

    # get the list of all possible 6-digit palindromes (there are 999)
    odo_entries = gen_odo_palindromes()

    # winnow the list by finding all 5-digit palindromes that are one
    # less than a 6-digit palindrome
    odo_five_entries = winnow_palindromes(odo_entries, 5)

    odo_four_entries = winnow_palindromes(odo_five_entries, 4)

    print("consider a 6 digit odometer w/o fractional miles")
    print("first entry is palidrome on last 4 digits")
    print("second entry is previous +1 mile and palindrome on last 5 digits")
    print("second entry is previous +1 mile and palindrome on all 6 digits")
    print()
    print("odometer readings that fit this pattern:")
    print()
    for entry in odo_four_entries:
        print(entry, str(int(entry) + 1).zfill(6), str(int(entry) + 2).zfill(6))
