# think python chapter 9, exercise 1

# read words.txt ands print only the words with more than 20 characters
# uses http://thinkpython2.com/code/words.txt


def read_words():
    """ read the word list in to memory """
    f = open("./words.txt", "r+")
    words = [line.rstrip() for line in f.readlines()]
    f.close()
    return words


def find_long_words(my_words):
    long_words = []
    max_word_len = 20
    for word in my_words:
        if len(word) > max_word_len:
            long_words.append(word)

    return long_words


if __name__ == "__main__":
    print()
    my_words = read_words()

    long_words = find_long_words(my_words)

    for word in long_words:
        print(word)
