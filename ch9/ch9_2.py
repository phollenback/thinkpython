# think python chapter 9, exercise 2

# read words.txt ands find the words with no 'e'
# then print the percentage of words that do not contain 'e'.

# uses http://thinkpython2.com/code/words.txt


def read_words():
    """ read the word list in to memory """
    f = open("./words.txt", "r+")
    words = [line.rstrip() for line in f.readlines()]
    f.close()
    return words


def has_no_e(word):
    """ return True if a word does not contain an 'e'. """

    for c in word:
        if c == "e":
            return False

    return True


def find_e_words(word_list):
    """ find all words in the word list that do not contain 'e'
    by calling has_no_e() on each word """

    no_e_list = []

    for word in word_list:
        if has_no_e(word):
            no_e_list.append(word)

    return no_e_list


if __name__ == "__main__":
    print()
    my_words = read_words()

    no_e_words = find_e_words(my_words)

    word_list_len = len(my_words)
    no_e_len = len(no_e_words)

    print("There are", word_list_len, "total words.")
    print("There are", no_e_len, "words that don't contain an 'e'.")

    print("{:.1%}".format(no_e_len / word_list_len), "of words don't contain an 'e'")
