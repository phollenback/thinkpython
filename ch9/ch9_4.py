# think python chapter 9, exercise 4

# given a word and a string of letters, return True if the
# word contains only letters on the list.


def uses_only(word, check_letters):
    """ return True if word uses only letters from check_letters """

    for c in word:
        if c not in check_letters:
            return False

    return True


def check_word(word, check_letters):
    """ wrapper function to check if a word only uses letters from
    check_letters"""

    if uses_only(word, check_letters):
        result = "contains only letters from"
    else:
        result = "contains letters NOT from"

    print(word, result, check_letters)


if __name__ == "__main__":
    print()

    check_word("powerful", "lufewop")
    check_word("culture", "erutluc")
    check_word("chronicle", "yxt")
