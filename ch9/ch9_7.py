# think python chapter 9, exercise 7

# find words with three consecutive pairs of double letters

# uses http://thinkpython2.com/code/words.txt


def contains_3pairs(word):
    """return True if a word contains three consecutive pairs of double
    letters.

    >>> contains_3pairs("bookkeeper")
    True
    >>> contains_3pairs('a')
    False
    >>> contains_3pairs('boottllicker')
    True
    >>> contains_3pairs("aabbc")
    False
    >>> contains_3pairs("abbccd")
    False
    >>> contains_3pairs("aabbrcc")
    False
    """

    if len(word) < 6:
        # 3 pairs so have to have at least 6 letters
        return False

    for i in range(len(word) - 5):
        if (
            word[i] == word[i + 1]
            and word[i + 2] == word[i + 3]
            and word[i + 4] == word[i + 5]
        ):
            return True

    return False


def read_words():
    """ read the word list in to memory """
    f = open("./words.txt", "r+")
    words = [line.rstrip() for line in f.readlines()]
    f.close()
    return words


def check_words(wordlist):
    """ check if each word has 3 adjacent pairs of double letters"""

    counter = 0
    for word in wordlist:
        if contains_3pairs(word):
            print(word)
            counter += 1

    return counter


if __name__ == "__main__":
    print()

    my_words = read_words()

    triples_count = check_words(my_words)
    print()
    print("there are", triples_count, "triple double words in the list")
