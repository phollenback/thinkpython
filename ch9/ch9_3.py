# think python chapter 9, exercise 3

# read words.txt ands find the word that don't contain a list
# of user-supplied forbidden letters.

# uses http://thinkpython2.com/code/words.txt


def get_forbidden_chars():
    """ get the list of forbidden characters from the user and
    return it """

    forbidden_chars = input("enter forbidden letters: ")

    return forbidden_chars


def avoids(word, forbidden_chars):
    """ check if a word contains the forbidden characters, return True
    if not. """

    for c in forbidden_chars:
        if c in word:
            return False

    return True


def read_words():
    """ read the word list in to memory """
    f = open("./words.txt", "r+")
    words = [line.rstrip() for line in f.readlines()]
    f.close()
    return words


def check_words(words, forbidden_chars):
    """ given a list of words and forbidden characters, check if any word
    contains a forbidden character. """

    my_good_word_count = 0

    for word in words:
        if avoids(word, forbidden_chars):
            my_good_word_count += 1

    return my_good_word_count


if __name__ == "__main__":
    print()

    my_words = read_words()

    my_forbidden_chars = get_forbidden_chars()

    good_word_count = check_words(my_words, my_forbidden_chars)

    print("found", good_word_count, "words that don't contain", my_forbidden_chars)
