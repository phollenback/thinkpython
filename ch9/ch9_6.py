# think python chapter 9, exercise 6

# write a function that checks if all the letters in a word are
# in alphabetical sequence.

# B) how many such words are there in the word list?

# uses http://thinkpython2.com/code/words.txt


def is_abcderian(word):
    """ return True if all letters in word are in alphabetical order """

    # use a counter to check each letter of a word against all
    # previous letters

    counter = 0
    for c in word:
        for d in range(0, counter):
            if ord(c) < ord(word[d]):
                return False
        counter += 1

    return True


def read_words():
    """ read the word list in to memory """
    f = open("./words.txt", "r+")
    words = [line.rstrip() for line in f.readlines()]
    f.close()
    return words


def check_words(wordlist):
    """ check that each word is abcderian and return total count"""

    counter = 0
    for word in wordlist:
        if is_abcderian(word):
            counter += 1

    return counter


if __name__ == "__main__":
    print()

    my_words = read_words()

    ord_count = check_words(my_words)
    print("there are", ord_count, "abcderian words in the list")
