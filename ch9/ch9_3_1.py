# think python chapter 9, exercise 3

# find a combination of 5 forbidden letters that excludes the
# smallest number of words from the word list

# uses http://thinkpython2.com/code/words.txt


def gen_forbidden_words():
    """generate a dict of all possible 5 character lowercase letter
    combinations"""

    forbidden_words = {}

    for a in range(97, 123):
        for b in range(97, 123):
            for c in range(97, 123):
                for d in range(97, 123):
                    for e in range(97, 123):
                        # use sort and join to make string unique
                        result = "".join(
                            sorted(set(chr(a) + chr(b) + chr(c) + chr(d) + chr(e)))
                        )
                        if len(result) == 5:
                            forbidden_words[result] = 1

    print(
        "created",
        len(forbidden_words),
        "unique 5 letter words without duplicate letters",
    )
    return forbidden_words


def avoids(word, forbidden_chars):
    """ check if a word contains the forbidden characters, return True
    if not. """

    for c in forbidden_chars:
        if c in word:
            return False

    return True


def read_words():
    """ read the word list in to memory """
    f = open("./words.txt", "r+")
    words = [line.rstrip() for line in f.readlines()]
    f.close()
    return words


def check_words(words, forbidden_chars):
    """ given a list of words and forbidden characters, check if any word
    contains a forbidden character.

    return count of words that don't contain forbidden characters """

    my_good_word_count = 0

    for word in words:
        if avoids(word, forbidden_chars):
            my_good_word_count += 1

    return my_good_word_count


if __name__ == "__main__":
    print()

    my_words = read_words()

    my_forbidden_words = gen_forbidden_words()

    lowest_word_count = len(my_words)

    least_exclusive_combo = ""

    for word in my_forbidden_words:
        word_count = check_words(my_words, word)
        if word_count < lowest_word_count:
            lowest_word_count = word_count
            least_exclusive_combo = word

    print(
        "the five letter combination",
        least_exclusive_combo,
        "excludes the smallest number of words:",
        lowest_word_count,
    )
