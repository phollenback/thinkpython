# think python chapter 8, exercise 3

# write a one line palindrome checker


def is_palindrome(word):
    if word[::-1] == word:
        return True
    else:
        return False


def check_palindrome(word):
    if is_palindrome(word):
        print(word, "is a palindrome")
    else:
        print(word, "is NOT a palindrome")


if __name__ == "__main__":
    print()
    check_palindrome("babab")
    check_palindrome("banana")
    check_palindrome("ableelba")
