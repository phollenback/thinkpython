# think python chapter 8, exercise 2

# use standard string count


def count_letters(word, letter):
    """ count the number of occurrences of letter 'letter' in
    word 'word' """

    my_count = word.count(letter)

    return my_count


if __name__ == "__main__":
    word = "banana"
    letter = "a"
    count = count_letters(word, letter)
    print("there are", count, "occurrences of the letter", letter, "in", word)
