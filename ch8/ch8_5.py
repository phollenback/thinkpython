# think python chapter 8, exercise 5

# write a rotX (ie rot13, etc) word rotator


def rotate_string(my_string, my_rot_amt):
    """rotate a string by integer amount (could be a negative amount)

    >>> rotate_string("",1)
    ''
    >>> rotate_string("test", 5)
    'yjxy'
    """

    rot_string = ""

    # only worry about the A-Z and a-z ranges:
    # A = 65, Z = 90
    # a = 97, z = 122

    # if ord is btw 65 and 90, wrap in that range for uppercase
    # same btw 97 and 122 for lowercase
    for c in my_string:
        chr_ord = ord(c)
        if 65 <= chr_ord <= 90:
            # this is an uppercase letter
            start_ord = 65
        elif 97 <= chr_ord <= 122:
            # this is a lowercase letter
            start_ord = 97
        else:
            # pass through unchanged
            rot_string += c
            continue

        # shift down to start at 0
        baseline = chr_ord - start_ord

        new_ord = ((baseline + my_rot_amt) % 26) + start_ord

        rot_string += chr(new_ord)

    return rot_string


if __name__ == "__main__":
    print()
    string = "You are my sunshinE"
    rot_amt = 13
    print(string)
    print("rotated", rot_amt)
    print(rotate_string(string, rot_amt))
