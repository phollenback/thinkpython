# think python chapter 6, exercise 5

# calculate greatest common divisor

# if r is remainder when a/b, then gcd(a,b) = gcd(b,r)
# base case: gcd(a/0) = a


def gcd(a, b):
    """ return the greatest common divisor between a and b
    >>> gcd(12,8)
    4
    >>> gcd(13,3)
    1
    """

    if b == 0:
        return a
    else:
        r = a % b
        return gcd(b, r)


def get_gcd(a, b):
    print("a:", a, "b:", b)
    print("  gcd is:", gcd(a, b))


if __name__ == "__main__":
    print()
    get_gcd(12, 8)
    get_gcd(13, 3)
    get_gcd(120, 50)
