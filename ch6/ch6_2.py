# think python chapter 6, exercise 2

# calculate the ackerman function

# A(m,n) = n + 1 if m = 0
# A(m,n) = A(m - 1, 1) if m > 0 and n = 0
# A(m,n) = A(m - 1, A(m, n - 1)) if m > 0 and n> 0

# ack(3,4) = 125


def ack(m, n):
    """ calculate ackerman function for two numbers
    >>> ack(3,4)
    125
    """

    if m == 0:
        return n + 1
    if m > 0 and n == 0:
        return ack((m - 1), 1)
    if m > 0 and n > 0:
        return ack(m - 1, ack(m, n - 1))


if __name__ == "__main__":
    print(ack(3, 4))
