# think python chapter 6, exercise 3

# palindrome solver


def first(word):
    """return fiurst letter of a word"""
    return word[0]


def last(word):
    """return last letter of a word"""
    return word[-1]


def middle(word):
    """return middle of word (first and last char removed)"""
    return word[1:-1]


def is_palindrome(test_word):
    """ test whether a word is a palindrome

    >>> is_palindrome("abba")
    True

    >>> is_palindrome("redivider")
    True

    >>> is_palindrome('')
    False

    >>> is_palindrome("abc")
    False
    """

    if len(test_word) == 0:
        # a word of length 0 is not a palindrome
        return False
    if first(test_word) != last(test_word):
        # if first and last don't match, obviously this is not a palindrome
        return False
    elif len(test_word) == 2:
        # if we fell thru from previous case and the length is 2, then we
        # are just left with a two word palindrome
        return True
    else:
        # recurse with first and last characters removed
        is_palindrome(middle(test_word))

    return True


def check_word(word):
    """just a little wrapper to call the palindrome function"""
    if is_palindrome(word):
        print(word, "is a palindrome")
    else:
        print(word, "is NOT a palindrome")


if __name__ == "__main__":
    """let's test some words to see if they are palindromes!"""

    check_word("turtle")
    check_word("abba")
    check_word("ele")
    check_word("")
    check_word("abcdefedcba")
    check_word("abcdeffedcba")
    check_word("thishereword")
