# think python chapter 6, exercise 4

# determine if a is a power of b

# a is a power of b if it's divisible by b and a/b is a power of b
# base case: a/b=1?


def is_power(a, b):
    """ determine if a is a power of b, recursively
    >>> is_power(26,5)
    False
    >>> is_power(81,3)
    True
    """

    if b == 1:
        # if b is 1 it jut divides forever until max recurse depth,
        # cause anything divided by 1 is itself.  Nip that in the bud.
        return False

    if a < b:
        # we've divided until we just can't any longer
        return False

    if a % b != 0:
        # any division that results in a remainder automatically
        # disqualifies
        return False

    if a / b == 1:
        # this is the ultimate end case for success
        return True

    if is_power(a / b, b):
        # recurse!
        return True

    return False


if __name__ == "__main__":
    a = 1024
    b = 4
    print(is_power(a, b))
