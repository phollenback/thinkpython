# think python chapter 10, exercise 9

# time two ways of appending to a list:
# 1. the append method
# 2. t = t + [x]

# use words.txt from previous chapters.

import timeit


def wrapper(func, *args, **kwargs):
    def wrapped():
        return func(*args, **kwargs)

    return wrapped


def read_words():
    """ read the word list in to memory """
    f = open("./words.txt", "r+")
    words = [line.rstrip() for line in f.readlines()]
    f.close()
    return words


def append_words(words):
    """ use append to create a list of all the words """
    wordlist = []

    for word in words:
        wordlist.append(word)


def add_words(words):
    """ use t = t + [x] to add words to list """
    wordlist = []

    for word in words:
        wordlist = wordlist + [word]


if __name__ == "__main__":

    print()
    my_words = read_words()
    print("testing appending")
    wrapped_append = wrapper(append_words, my_words)
    append_time = timeit.timeit(wrapped_append, number=1)
    print("testing addition")
    wrapped_add_word = wrapper(add_words, my_words)
    add_time = timeit.timeit(wrapped_add_word, number=1)

    print()
    print(f"time with append: {append_time:.4f} seconds")
    print(f"time with addition: {add_time:.4f} seconds")
