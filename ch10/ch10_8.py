# think python chapter 10, exercise 8

# test the birthday paradox - generate random samples of 23 birthdays and check
# if there are any matches

import random


def gen_birthdays():
    """ generate a list of 23 birthdays.  Return a list of lists in format
    [[month, day]],[month,day]] - first item in each list is an int between 1 and
    12 for the month, and the second is an int between 1 and 31 for the day """

    bday_list = []

    for _ in range(23):
        month = randint(1, 12)

        if month == 2:
            daybound = 28
        elif month in [1, 3, 5, 7, 8, 10, 12]:
            daybound = 31
        else:
            daybound = 30

        day = randint(1, daybound)

        bday_list.append([month, day])

    return bday_list


def check_for_matches(bday_list):
    """ given a list of pairs of dates, return True if there are any dupes

    this is not very efficient """

    for (m, n) in bday_list:
        if bday_list.count([m, n]) > 1:
            return True

    return False


if __name__ == "__main__":

    print()
    run_counter = 10000
    match_counter = 0

    print("generating", run_counter, "sets of 23 birthdays")

    for _ in range(run_counter):
        bday_list = gen_birthdays()
        if check_for_matches(bday_list):
            match_counter += 1

    print(
        "found matching birthdays", match_counter, "times.",
    )
    print("that's", "{0:.0%}".format(match_counter / run_counter), "of the time!")
