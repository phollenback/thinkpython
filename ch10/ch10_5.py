# think python chapter 10, exercise 5

# write a function called is_sorted that takes a list and returns True iof the
# list is sorted in ascending order.


def is_sorted(t):
    """ determine if a list is sorted in ascending order """

    prev = t[0]
    for n in enumerate(t):
        if prev > n[1]:
            return False
        prev = n[1]

    return True


def check_if_sorted(t):
    """ convenience wrapper for is_sorted """

    if is_sorted(t):
        return True

    return False


if __name__ == "__main__":

    print()

    list1 = [1, 2, 3, 4, 5, 6]
    list2 = [1, 5, 3, 11, 55, 7]

    for t in list1, list2:
        if check_if_sorted(t):
            print(t, "is sorted")
        else:
            print(t, "is NOT sorted")
