# think python chapter 10, exercise 3

# write a function called middle thnat takes a list and returns a new list
# containing all but the first and last elements of the original list.


def middle(t):
    """chop off the first and last elements of a list and return the
    middle"""

    return t[1:-1]


if __name__ == "__main__":

    print()

    my_list = [1, 2, 3, 4, 5, 6]

    print("middle of", my_list, ":")
    print(middle(my_list))
