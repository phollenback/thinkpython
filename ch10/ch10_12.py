# think python chapter 10, exercise 12

# find all interlocking words in the word list.  Example: shoe and cold
# interlock to form 'schooled' - alternating letters from each forms a
# valid new word.

# use words.txt from previous chapters.

def read_words():
    """ read the word list in to memory """
    f = open("./words.txt", "r+")
    words = [line.rstrip() for line in f.readlines()]
    f.close()
    return words


def binary_search(word, wordlist):
    """ return True if word is in wordlist, false otherwise.
    wordlist must be in alphabetical order because this is a bisection
    search """

    left = 0
    right = len(wordlist) - 1
    mid = (right + left) // 2

    while wordlist[mid] != word:
        if mid == right:
            # we have searched the whole list, give up
            return False
        if wordlist[mid] < word:
            left = mid + 1
        else:
            right = mid - 1

        mid = (right + left) // 2

    return True

def find_interlock_words(wordlist):
    """ take apart each word in the wordlist and see if it breaks down
    in to two words that exist in the wordlist. """

    interlocks = []
    
    for word in wordlist:
        # get the potential interlock words by slicing the string
        even = word[::2]
        odd = word[1::2]

        if (binary_search(even, wordlist) and
            binary_search(odd, wordlist)):
                # the two subparts are in the wordlist, so this word
                # is a valid interlock
                interlocks.append([even, odd, word])

    return interlocks

def find_triple_interlocks(wordlist):
    """ take apart each word in wordlist and see if it breaks down
    in to three words that exist in the wordlist """

    interlocks = []
    
    for word in wordlist:
        # get the 3 potential words
        first = word[::3]
        second = word[1::3]
        third = word[2::3]

        if (binary_search(first, wordlist) and
            binary_search(second, wordlist) and
            binary_search(third, wordlist)):
                # the three subparts are in the wordlist, so this word
                # is a valid interlock
                interlocks.append([first, second, third, word])

    return interlocks

if __name__ == "__main__":
    print()
    my_words = read_words()
    #my_words = "cold", "schooled", "shoe"
    interlocked = find_interlock_words(my_words)
    for iword in interlocked:
        print(iword[0], iword[1], iword[2])

    print()

    print("found",
          len(interlocked),
          "pairs of interlock words out of",
          len(my_words),
          "total words")

    interlock3 = find_triple_interlocks(my_words)
    for iword in interlock3:
            print(iword[0], iword[1], iword[2], iword[3])

    print()
    
    print("found",
          len(interlock3),
          "sets of triple interlocked words out of",
          len(my_words),
          "total words")
