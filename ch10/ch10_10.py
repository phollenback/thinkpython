# think python chapter 10, exercise 10

# bisection search.  given a list of words and a word to check, return either
# None if the word is in the list, or the index of the check word in the list.


# use words.txt from previous chapters.

# I never did quite figure out how to deal with rounding errors making the
# index slightly wrong.  Instead, I "fixed" it by padding the list to a length
# of a power of two.  That way every division has no remainder and we get the
# right results.  This is slightly inefficient but does work.

import math


def read_words():
    """ read the word list in to memory """
    f = open("./words.txt", "r+")
    words = [line.rstrip() for line in f.readlines()]
    f.close()
    return words


def in_bisect(wl, checkword, index):
    """wordlist is assumed to be already alphabetically sorted.
    determine if checkword is in wordlist.  return None if not in list,
    list index if in list.
    """

    # check for the empty list
    if len(wl) == 0:
        print("wordlist is empty")
        return None

    # we will add or subtract the offset to track the index as we recurse -
    # offset is half of half of current list length
    offset = len(wl) // 4

    # check against the word in the middle of the current list
    testword = wl[len(wl) // 2]

    # is the word in the middle of the current list a match?
    if checkword == testword:
        return index

    # if we got this far and there's only one word in the list, we
    # know it can't be the correct word.
    if len(wl) == 1:
        return None

    # next two sections either bisect to the left or right, depending
    # on alphabetical sorting.

    if checkword < testword:
        # subtract offset since we are going backward
        index -= offset
        bisect = len(wl) // 2
        return in_bisect(wl[:bisect], checkword, index)

    if checkword > testword:
        # add offset since we are going forward
        index += offset
        bisect = len(wl) // 2
        return in_bisect(wl[bisect:], checkword, index)

    # we never reach this return but it seems best to include
    # for clarity
    return None


def pad_to_twos(my_words):
    """ add extra list entries to end of list to get it to
    exactly a power of twos length """

    i = 2
    list_len = len(my_words)
    while i < list_len:
        i = i * 2
    pad_len = i - list_len
    for i in range(pad_len):
        my_words.append("zzzz")

    return my_words


if __name__ == "__main__":

    print()
    my_words = read_words()
    wl_len = len(my_words)
    my_words = pad_to_twos(my_words)

    checkwords = ["bar", "cage", "elephant", "infrument", "mariachi", "goo"]

    print("checking", len(checkwords), "words against a list of", wl_len, "words")
    print()
    for checkword in checkwords:
        # note that starting index is the index of the middle word
        # of the full word list
        my_index = in_bisect(my_words, checkword, (len(my_words) // 2))
        if my_index is None:
            diff = None
            print(checkword, "is not in list")
        else:
            print(checkword, "in list at index", my_index)
