# think python chapter 10, exercise 6

# anagram solver - are two words anagrams (same letters but transposed.)


def is_anagram(word1, word2):
    """ return True if two words are anagrams """

    if len(word1) != len(word2):
        # obviously two words of different lengths can't be anagrams
        return False

    sort1 = sort(list(word1))
    sort2 = sort(list(word2))

    for (n, m) in enumerate(sort1):
        if m != sort2[n]:
            return False

    return True


if __name__ == "__main__":

    print()

    wordsets = (["bubmble", "bbumble"], ["arrow", "longsword"], ["a", "a"])

    for (my_word1, my_word2) in wordsets:
        if is_anagram(my_word1, my_word2):
            outcome = "is an anagram of"
        else:
            outcome = "is NOT an anagram of"

        print(my_word1, outcome, my_word2)
