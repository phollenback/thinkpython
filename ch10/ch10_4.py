# think python chapter 10, exercise 4

# write a function that modifies a list by removing the first and last elements
# and returning None. ie actually modify the list, don't make a copy


def chop(t):
    """chop off the first and last elements of a list, operate in place,
    and return None"""

    t.pop()
    t.pop(0)

    return None


if __name__ == "__main__":

    print()

    my_list = [1, 2, 3, 4, 5, 6]

    print("my list: ", my_list)
    chop(my_list)
    print("same list, chopped: ", my_list)
