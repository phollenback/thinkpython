# think python chapter 10, exercise 7

# has_duplicates - take a list and return True if the list contains
# any duplicate elements.


def has_duplicates(testlist):

    sorted_list = sort(testlist)

    old_item = None

    for i in sorted_list:
        if i == old_item:
            return False
        old_item = i

    return True


if __name__ == "__main__":

    print()

    testlists = (
        [1, 2, 3, 4, 5, 6, 7, 8],
        ["a", "b", "c", "d", "d", "e"],
        ["foo", "bar", "baz"],
        ["bar", "bar", "foo"],
    )

    for my_list in testlists:
        if has_duplicates(my_list):
            outcome = "does NOT have duplicates"
        else:
            outcome = "has duplicates"

        print(my_list, outcome)
