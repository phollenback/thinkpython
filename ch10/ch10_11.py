# think python chapter 10, exercise 11

# find all words in the word list that are 'reverse pairs' - one is the
# reverse of the other.

# use words.txt from previous chapters.

from timeit import default_timer as timer


def read_words():
    """ read the word list in to memory """
    f = open("./words.txt", "r+")
    words = [line.rstrip() for line in f.readlines()]
    f.close()
    return words


def binary_search(word, wordlist):
    """ return True if word is in wordlist, false otherwise.
    wordlist must be in alphabetical order because this is a bisection
    search """

    left = 0
    right = len(wordlist) - 1
    mid = (right + left) // 2

    while wordlist[mid] != word:
        if mid == right:
            # we have searched the whole list, give up
            return False
        if wordlist[mid] < word:
            left = mid + 1
        else:
            right = mid - 1

        mid = (right + left) // 2

    return True


def find_rev_pairs(wordlist):
    """find and returns all words in given word list that are
    reverse pairs"""

    rev_pairs = []

    for word in wordlist:
        revword = word[::-1]
        pair = [word, revword]
        revpair = [revword, word]
        if word == revword:
            # ignore palindromes
            pass
        elif binary_search(revword, wordlist):
            # we've found a reverse pair. make sure we haven't already
            # added it.
            if revpair not in rev_pairs:
                rev_pairs.append(pair)

    return rev_pairs


if __name__ == "__main__":

    print()
    my_words = read_words()
    # my_words = "abc", "bab", "cba", "ddff", "ffdd", "maple", "zebra", "arbez"
    start = timer()
    my_rev_pairs = find_rev_pairs(my_words)
    end = timer()
    print("found", len(my_rev_pairs), "reverse pairs in", len(my_words), "words")
    print("elapsed time", end - start, "seconds")
