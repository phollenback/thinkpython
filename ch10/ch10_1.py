# think python chapter 10, exercise 1

# write a function called nested_sum that takes a list of lists of
# integers and adds up the elements from all the nested lists

# t = [[1,2],[3],[4,5,6]]
# 21

# bonus question I thought up: do this for arbitrarily nested lists


def nested_sum(t):
    """ compute sum of integers in nested lists """

    result = 0
    for i in t:
        for j in i:
            result += j

    return result


def nested_sum_general(t):
    """compute sum of integers in nested lists.  unlike nested_sum(),
    deal with arbitrarily deep nesting, for example:

    [[1,2,[3]],[4,[5,[6]]]] """

    sum = 0
    for item in t:
        if isinstance(item, int):
            sum += item
        else:
            sum += nested_sum_general(item)

    return sum


if __name__ == "__main__":

    print()

    my_list = [[1, 2], [3], [4, 5, 6]]
    my_worse_list = [[1, 2, [3]], [4, [5, [6]]]]

    print("adding integers in nested lists:")
    print()
    print("using simple function that can deal with one level of nesting:")
    print(my_list)
    print(nested_sum(my_list))
    print("using recursive function that can deal with arbitrary levels of nesting:")
    print(my_list)
    print(nested_sum_general(my_list))
    print("using recursive function with deeply nested list:")
    print(my_worse_list)
    print(nested_sum_general(my_worse_list))
