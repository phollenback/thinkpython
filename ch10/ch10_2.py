# think python chapter 10, exercise 2

# write a function caled cumsum that takes a list of numbers and returns the
# cumulative sum; that is, a new list where the ith element is the sum of the
# first i+1 elements from the original list.

# t = [1,2,3]
# cumsum(t) = [1,3,6]


def cumsum(t):
    """ cumulative sum of a list

    >>> cumsum[1,2,3]
    [4, 5, 6]
    >>> cumsum[1]
    [1]
    """

    summer = 0
    sumlist = []

    for i in t:
        summer = i + summer
        sumlist.append(summer)

    return sumlist


if __name__ == "__main__":

    print()

    my_list = [1, 2, 3]

    print("cumulative sum of", my_list, ":")
    print(cumsum(my_list))
