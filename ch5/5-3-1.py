# think python chapter 5, exercise 3 part 1

# function to take three ints as arguments and
# determines if they form a valid triangle.

# if any of the three lengths are greater than the sum of the other two, you
# cannot form a triangle.


def is_triangle(a, b, c):
    """ determine if a valid triangle can be created.

    input: a - int
    input: b - int
    input: c - int

    >>> is_triangle(1, 1, 12)
    False
    >>> is_triangle(7,10,5)
    True
    """

    if a + b <= c or a + c <= b or b + c <= a:
        return False

    return True


if __name__ == "__main__":

    if is_triangle(3, 4, 3):
        print("Yes")
