# think python chapter 5, exercise 3 part 2

# use module from previous part to determine if three user-inputed
# integers form a valid triangle.

import istriangle


def get_input():
    """get user input of three integers for the triangle"""

    a = int(input("enter side a: "))
    b = int(input("enter side b: "))
    c = int(input("enter side c: "))

    return a, b, c


if __name__ == "__main__":

    # my_a, my_b, my_c = get_input()

    # note use of splat operator here to unpack arguments from get_input
    if istriangle.is_triangle(*get_input()):
        print("that's a triangle!")
    else:
        print("sorry charlie!")
