# think python chapter 5, exercise 6 part 2

# call koch curve function from 5-6-1 to draw a snowflake

import turtle
import koch


def snowflake(t, side_len):
    """ draw a snowflake by drawing 3 koch curves """

    for i in range(0, 3):
        koch.koch(t, side_len)
        print("turning 120 degrees")
        t.rt(120)


if __name__ == "__main__":
    bob = turtle.Turtle()

    # warp speed!
    bob.speed(0)

    flake_len = 100
    print()
    print("drawing a snowflake of side length", flake_len)
    snowflake(bob, 100)
