# think python chapter 5, exercise 6 part 1

# draw a koch curve with recursion

import turtle


def koch(t, length):
    """ recursively draw a koch curve"""

    if length < 3:
        t.fd(length)
        return

    koch(t, length / 3)
    t.lt(60)
    koch(t, length / 3)
    t.rt(120)
    koch(t, length / 3)
    t.lt(60)
    koch(t, length / 3)


if __name__ == "__main__":

    bob = turtle.Turtle()

    # warp speed!
    bob.speed(0)

    curve_len = 100
    print()
    print("drawing a koch curve of length", curve_len)
    koch(bob, 100)
