# think python chapter 5, exercise 2 part 2

# building on 5-2, ask user to input numbers to test for
# fermat's last theorem.

import fermat


def get_input():
    """ get 4 values for use testing fermat's last theorem"""

    a = input("enter a: ")
    b = input("enter b: ")
    c = input("enter c: ")
    n = input("enter n: ")

    return int(a), int(b), int(c), int(n)


if __name__ == "__main__":

    # get values to test
    my_a, my_b, my_c, my_n = get_input()

    if fermat.check_fermat(my_a, my_b, my_c, my_n):
        print("Holy smokes! Fermat was wrong!")
    else:
        print("No, that doesn't work")
