# think python chapter 5, exercise 1

# convert unix epoch time to human-readable time of
# hours, minutes, seconds plus days since epoch start

# this is a really ugly implementation because I am just learning about
# python classes please be gentle

import time


class TimeThing:
    """encapsulate a time calculation.  when instantiated, either accepts
    an epoch seconds value, or get the current epoch seconds value.

    contains private methods as dunders to actually do calculations.  I
    did it this way because some levels of response requires output of
    previous level.  For example, to calculate the day of the month, you
    need to know how many seconds have elapsed this month, which means
    you need to know how many months have elapsed this year, etc.

    I'm sure there are much better ways to handle this.
    """

    def __init__(self, epoch_time=time.time()):
        """set the epoch time to the current epoch time if no value is
        supplied"""
        self.epoch_time = epoch_time

    def __str__(self):
        """ by default, print the actual epoch time we are using"""
        return str(self.epoch_time)

    # number of seconds in one day
    SECS_PER_DAY = 24 * 60 * 60

    # number of seconds in one hour
    SECS_PER_HOUR = 60 * 60

    def __year_is_leap(self, this_year):
        """ determine if a given year is a leap year, return True if so
        >>> TimeThing(...)._TimeThing__year_is_leap(2000)
        True
        >>> TimeThing(...)._TimeThing__year_is_leap(1972)
        True
        >>> TimeThing(...)._TimeThing__year_is_leap(1900)
        False
        >>> TimeThing(...)._TimeThing__year_is_leap(2019)
        False
        """

        if this_year % 4 == 0 and this_year % 100 != 0:
            return True
        if this_year % 400 == 0:
            return True

        return False

    def __determine_year_helper(self, epoch_time):
        """determine year, return year and number of seconds elapsed in
        the year
        >>> TimeThing(...)._TimeThing__determine_year_helper(0)
        (1970, 0)
        >>> TimeThing(...)._TimeThing__determine_year_helper(1)
        (1970, 1)
        >>> TimeThing(...)._TimeThing__determine_year_helper(1574374238)
        (2019, 28073438)
        """

        leap_year_secs = self.SECS_PER_DAY * 366
        regular_year_secs = self.SECS_PER_DAY * 365

        # use a countup value
        epoch_counter = 0

        # the very first year was of course 1970.
        this_year = 1970

        # this loop continues until adding another year would exceed the number
        # of seconds in epoch_time
        while True:
            if self.__year_is_leap(this_year):
                if epoch_counter + leap_year_secs > epoch_time:
                    return this_year, epoch_time - epoch_counter
            else:
                if epoch_counter + regular_year_secs > epoch_time:
                    return this_year, epoch_time - epoch_counter

            if self.__year_is_leap(this_year):
                epoch_counter += leap_year_secs
            else:
                epoch_counter += regular_year_secs

            this_year += 1

    def determine_year(self):
        this_year, _ = self.__determine_year_helper(self.epoch_time)
        return this_year

    def __determine_month_helper(self, this_year, secs_this_year):
        """ determine which month we are currently in
        takes the year as input because we have to check if it's a leap year.
        returns the month number (january = 1) and the number of seconds elapsed
        in the current month

        >>> TimeThing(...)._TimeThing__determine_month_helper(1970,0)
        (1, 0)
        >>> TimeThing(...)._TimeThing__determine_month_helper(2019,15000000)
        (6, 1953600)
        """

        month_sec_counter = 0
        prev_month_sec_counter = 0

        if self.__year_is_leap(this_year):
            feb_days = 29
        else:
            feb_days = 28

        days_per_month = (31, feb_days, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

        month_counter = 1
        for m in days_per_month:
            secs_this_month = m * 24 * 60 * 60
            prev_month_sec_counter = month_sec_counter
            month_sec_counter += secs_this_month

            if month_sec_counter > secs_this_year:
                # we've found the month (gone past secs this year)
                return (month_counter, secs_this_year - prev_month_sec_counter)

            month_counter += 1

    def determine_month(self):
        """determine month by calling helper"""
        this_year, secs_this_year = self.__determine_year_helper(self.epoch_time)
        this_month, _ = self.__determine_month_helper(this_year, secs_this_year)

        return this_month

    def __determine_day_of_month_helper(self, secs_this_month):
        """determine which day of the month it is. Takes the elapsed number of
        seconds this month as input, returns the day of the month and number of
        seconds elapsed in current day

        >>> TimeThing(...)._TimeThing__determine_day_of_month_helper(0)
        1
        >>> TimeThing(...)._TimeThing__determine_day_of_month_helper(90000)
        2
        """

        # because the first day of the month is 1, not 0.
        day_of_month = (secs_this_month // self.SECS_PER_DAY) + 1
        return day_of_month

    def determine_day_of_month(self):
        """determine day of month by calling helper"""
        this_year, secs_this_year = self.__determine_year_helper(self.epoch_time)
        _, secs_this_month = self.__determine_month_helper(this_year, secs_this_year)
        day_of_month = self.__determine_day_of_month_helper(secs_this_month)
        return int(day_of_month)

    def __determine_hour_of_day_helper(self, secs_today):
        """given the number of seconds that have elapsed today, determine
        what the curent hour is.  return hour of day and number of seconds
        elapsed this hour.  Note: 24-hour time format, midnight is 0.

        >>> TimeThing(...)._TimeThing__determine_hour_of_day_helper(0)
        0
        >>> TimeThing(...)._TimeThing__determine_hour_of_day_helper(3601)
        1
        >>> TimeThing(...)._TimeThing__determine_hour_of_day_helper(86359)
        23
        """

        return int(secs_today // self.SECS_PER_HOUR)

    def determine_hour_of_day(self):
        """determine hour of day by calling helper"""
        secs_today = self.epoch_time % self.SECS_PER_DAY
        hour_of_day = self.__determine_hour_of_day_helper(secs_today)

        return hour_of_day

    def __determine_minute_of_hour_helper(self, secs_this_hour):
        """given the nuber of seconds that have elapsed this hour, determine
        what the current minute is. Returns minute of current hour and
        number of seconds that have elapsed this minute.

        >>> TimeThing(...)._TimeThing__determine_minute_of_hour_helper(0)
        0
        >>> TimeThing(...)._TimeThing__determine_minute_of_hour_helper(3599)
        59
        """

        return secs_this_hour // 60

    def determine_minute_of_hour(self):
        """determine minute of hour by calling helper"""

        secs_this_hour = self.epoch_time % self.SECS_PER_HOUR
        minute_of_hour = self.__determine_minute_of_hour_helper(secs_this_hour)

        return int(minute_of_hour)

    def __determine_secs_of_minute_helper(self, secs_this_hour):
        """how may seconds have elapsed this minute

        >>> TimeThing(...)._TimeThing__determine_secs_of_minute_helper(0)
        0
        >>> TimeThing(...)._TimeThing__determine_secs_of_minute_helper(59)
        59
        """

        return int(secs_this_hour % 60)

    def determine_secs_of_minute(self):
        """determine second of minute"""

        secs_this_hour = self.epoch_time % self.SECS_PER_HOUR

        secs_this_minute = self.__determine_secs_of_minute_helper(secs_this_hour)
        return secs_this_minute


if __name__ == "__main__":

    print()

    # create my time object, using default of current epoch seconds
    MyTime = TimeThing()

    # get the time values
    year = MyTime.determine_year()
    month = MyTime.determine_month()
    day = MyTime.determine_day_of_month()
    hour = MyTime.determine_hour_of_day()
    minute = MyTime.determine_minute_of_hour()
    seconds = MyTime.determine_secs_of_minute()

    # print the time values
    print("epoch seconds:", MyTime)
    print()
    print("year:", year)
    print("month:", month)
    print("day:", day)
    print("hour:", hour)
    print("minute:", minute)
    print("seconds:", seconds)
