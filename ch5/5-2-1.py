# think python chapter 5, exercise 2 part 1

# write a function to test fermat's last theorem

# there exists no positive integers a,b,c where a^n + b^n = c^n
# for any value of n greater than 2

import math


def check_fermat(a, b, c, n):
    """ check whether fermat's last theorem holds - given positive
    integers a,b,c there is no n > 2 where a^n + b^n = c^n.

    Return True if function actually works, False otherwise

    >>> check_fermat(1,2,3,4)
    False
    """

    if math.pow(a, n) + math.pow(b, n) == math.pow(c, n):
        return True

    return False


if __name__ == "__main__":

    if check_fermat(1, 2, 3, 4):
        print("Holy smokes! Fermat was wrong!")
    else:
        print("No, that doesn't work")
