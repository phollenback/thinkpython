# think python chapter 13, exercise 2

# Read a text file and create a histogram for the words in it.

import string

histogram = {}


def update_histogram(word):
    """ update the global histogram for the give word """
    if word in histogram:
        histogram[word] += 1
    else:
        histogram[word] = 1


def read_word_file():
    """ read a file of words and process it """

    f = open("IgorizationWriteup.txt", "r+")
    for line in f.readlines():
        new_line = ""
        words = line.split()
        for word in words:
            new_word = ""
            for c in word:
                # rule out punc, whitespace, and digits
                if c in string.punctuation:
                    continue
                if c in string.digits:
                    continue
                if c in string.whitespace:
                    continue
                # now lowercase
                l = c.lower()

                new_word += l

            update_histogram(new_word)

    f.close()


def count_total_words():
    """ add up the counts for each word to get the total number of words """
    word_count = 0

    for word in histogram:
        word_count += histogram[word]

    return word_count


def find_most_common_word():
    """ determine most common word in the histogram """
    freq_count = 0

    for word in histogram:
        if histogram[word] > freq_count:
            freq_count = histogram[word]
            most_freq_word = word

    return (most_freq_word, freq_count)


if __name__ == "__main__":
    print()

    read_word_file()
    number_of_words = len(histogram)
    print("word file contained", count_total_words(), "total words")
    print("word file contained", number_of_words, "different words")
    (most_freq, count) = find_most_common_word()
    print("the most common word was \'", most_freq, "\' with", count, "occurrences")
