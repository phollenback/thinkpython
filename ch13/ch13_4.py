# think python chapter 13, exercise 4

# Modify ch13 ex 3 to compare a text file to a wordlist and print
# all the words in the file that are not in the word list.


import string

histogram = {}


def update_histogram(word):
    """ update the global histogram for the given word """
    if word in histogram:
        histogram[word] += 1
    else:
        histogram[word] = 1


def read_word_list():
    """ read the word list in to a dict """
    words = {}
    # default word list does not include single letter words or
    # empty string, so add them.
    words["i"] = 1
    words["a"] = 1
    f = open("./words.txt", "r+")
    for line in f.readlines():
        word = line.rstrip()
        words[word] = 1
    f.close()
    return words


def read_word_file():
    """ read a file of words and process it """

    f = open("IgorizationWriteup.txt", "r+")
    for line in f.readlines():
        words = line.split()
        for word in words:
            new_word = ""
            for c in word:
                # rule out punc, whitespace, and digits
                if c in string.punctuation:
                    continue
                if c in string.digits:
                    continue
                if c in string.whitespace:
                    continue
                # now lowercase
                l = c.lower()

                new_word += l

            update_histogram(new_word)

    f.close()


if __name__ == "__main__":
    unique_words = []

    print()
    my_wordlist = read_word_list()
    read_word_file()

    for word in histogram:
        if word not in my_wordlist:
            unique_words.append(word)

    print("wordlist words not in text file:")
    print()
    print(unique_words)
