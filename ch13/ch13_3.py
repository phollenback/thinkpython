# think python chapter 13, exercise 3

# Modify ch13 ex 2 to print the 20 most common words in the
# word histogram for a text file.


import string

histogram = {}


def update_histogram(word):
    """ update the global histogram for the given word """
    if word in histogram:
        histogram[word] += 1
    else:
        histogram[word] = 1


def read_word_file():
    """ read a file of words and process it """

    f = open("IgorizationWriteup.txt", "r+")
    for line in f.readlines():
        words = line.split()
        for word in words:
            new_word = ""
            for c in word:
                # rule out punc, whitespace, and digits
                if c in string.punctuation:
                    continue
                if c in string.digits:
                    continue
                if c in string.whitespace:
                    continue
                # now lowercase
                l = c.lower()

                new_word += l

            update_histogram(new_word)

    f.close()


def invert_dict(a_dict):
    """ invert a dict using setdefault """
    inverse = dict()
    for key in a_dict:
        val = a_dict[key]
        inverse.setdefault(val, []).append(key)
    return inverse


if __name__ == "__main__":
    print()
    print("most common words in word file:")
    print()
    # print top 'n' most common
    n = 5
    read_word_file()
    inverted = invert_dict(histogram)
    sorted_keys = sorted(inverted)
    for i in sorted_keys[::-n]:
        print(inverted[i])
