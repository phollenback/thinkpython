# think python chapter 13, exercise 1

# Read a file, break each line in to words, strip whitespace and punctuation,
# and convert to lowercase.

import string


def read_word_file():
    """ read a file of words and process it """

    f = open("IgorizationWriteup.txt", "r+")
    for line in f.readlines():
        new_line = ""
        words = line.split()
        for word in words:
            new_word = ""
            for c in word:
                # rule out punc, whitespace, and digits
                if c in string.punctuation:
                    continue
                if c in string.digits:
                    continue
                if c in string.whitespace:
                    continue
                # nbow owercase and add to new output line
                l = c.lower()
                new_word += l
            if len(new_word) > 0:
                new_line += new_word
                new_line += " "

        if len(new_line) > 0:
            print(new_line.strip())

    f.close()


if __name__ == "__main__":
    print()

    read_word_file()
