# think python chapter 4, exercise 2

# turtle code to create arbitrary flowers

import turtle


def arc(t, r, angle):
    """ draw an arc using turtle t of radius r and
    <angle> defrees of the complete circle"""
    arc_length = 2 * math.pi * r * angle / 360
    n = int(arc_length / 3) + 1
    step_length = arc_length / n
    step_angle = angle / n

    for i in range(n):
        t.fd(step_length)
        t.lt(step_angle)


def petal(t, r, angle):
    """ draw a petal using turtle t of radius r and
    <angle> degrees of the complete circle"""
    for i in range(2):
        arc(t, r, angle)
        t.lt(180 - angle)


def flower(t, n, r, angle):
    """ draw a flower using turtle t with n petals
    r is the radius of the completed flower and
    <angle> is the slice of a circle to use"""
    for i in range(n):
        petal(t, r, angle)
        t.lt(360.0 / n)


bob = turtle.Turtle()

flower(bob, 7, 100, 60)
flower(bob, 9, 120, 20)
