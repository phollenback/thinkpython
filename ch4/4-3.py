# think python chapter 4, exercise 3

# draw polygon pie

import turtle


def calc_side(r, n, slice_angle):
    """given radius and number of sides of regular polygon, return the
    length of a side

    this works by treating every side as a triangle that you can split
    in half for two right triangles - then you can use trigonometrty to
    calculate the length of each side, based on the radius.
    """

    # trigonometry time!
    # notice conversion to radians, apparently that's default in python?
    side_sin = math.sin(math.radians(slice_angle / 2))
    side_len = side_sin * r * 2

    print()
    print("num sides:", n, "side length:", side_len)
    return side_len


def draw_slices(t, r, n):
    """draw lines from center to each vertex to create pie slices"""

    # calculate vertex angles
    vertex_angle = ((n - 2) * 180) / n

    # slice_angle is the center vertex angle of each equilateral triangle
    # that forms a side
    slice_angle = 360.0 / n

    # turn enough to point at first vertex
    if n % 2 == 0:
        t.rt(slice_angle)
    else:
        t.rt(slice_angle / 2)

    for i in range(n):
        # turn to point at first vertex
        t.lt(vertex_angle)
        # draw from center to vertex
        t.fd(r)
        # go back to center
        t.penup()
        t.lt(180)
        t.fd(r)
        # don't forget to put the pen down
        t.pendown()


def draw_sides(t, r, n):
    """ draw the sides of the polygon """

    # calculate vertex angles
    vertex_angle = ((n - 2) * 180) / n

    # slice_angle is the center vertex angle of each equilateral triangle
    # that forms a side
    slice_angle = 360.0 / n

    # turn enough to point at first vertex
    if n % 2 == 0:
        t.rt(slice_angle)
    else:
        t.rt(slice_angle / 2)

    # go out to first vertex
    t.penup()
    t.fd(r)
    t.pendown()

    side_length = calc_side(r, n, slice_angle)

    t.lt(vertex_angle / 2)
    for i in range(n):
        t.lt(180 - vertex_angle)
        t.fd(side_length)


def polygon_pie(t, r, n):
    """ draw a polygon pie of n sides with radius r using turtle t"""

    # reset to initial settings
    bob.setheading(90)
    bob.penup()
    bob.setx(0)
    bob.sety(0)
    bob.pendown()

    # draw spokes and sides
    draw_slices(t, r, n)
    draw_sides(t, r, n)


def change_color(t):
    R = random.random()
    B = random.random()
    G = random.random()
    t.color(R, G, B)


bob = turtle.Turtle()
bob.speed(0)

# main loop - draw various polygons in random colors
for i in range(3, 30):
    change_color(bob)
    polygon_pie(bob, i * 15, i)
