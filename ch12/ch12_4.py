# think python chapter 12, exercise 4

# Find the longest word that remains a valid word each time you remove
# one letter.  Each time any letter can be removed, but you can't
# reorder the letters.

from typing import Dict

reducible_words = {}  # type: Dict[str, int]


def read_words():
    """ read the word list in to a dict """

    words = {}

    # default word list does not include single letter words or
    # empty string, so add them.
    words["i"] = 1
    words["a"] = 1
    words[""] = 1

    f = open("./words.txt", "r+")
    for line in f.readlines():
        word = line.rstrip()
        words[word] = 1

    f.close()
    return words


def find_children(word, words):
    """ find all the valid children of a word - that is, all cases where
    removing one letter from the word results in a valid word """

    test_child = ""
    valid_children = []

    for i, _ in enumerate(word):
        test_child = word[:i] + word[i + 1 :]
        if test_child in words:
            valid_children.append(test_child)

    return valid_children


def recurse_children(word, words):
    """ recursively compile all valid children of a word, by repeatedly
    calling find_children """

    # base case - the empty string
    if word == "":
        return True

    children = find_children(word, words)
    for child in children:
        if child in reducible_words:
            return True

        return recurse_children(child, words)

    # we fell through, original word is not reducible
    return False


if __name__ == "__main__":
    print()

    my_words = read_words()

    for my_word in my_words:
        if recurse_children(my_word, my_words):
            reducible_words[my_word] = 1

    print("found", len(reducible_words), "reducible words")

    length = 0
    longest_reducible = ""

    for my_word in reducible_words:
        if len(my_word) > length:
            longest_reducible = my_word
            length = len(my_word)

    print("longest reducible word:", longest_reducible)
