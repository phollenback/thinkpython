#!/usr/bin/python3

# think python chapter 12, exercise 3

# Find all the metathesis pairs in a word list.  Metathesis pairs are
# words with two letters swapped, such as 'conserve' and 'converse'.

# Since the length of the word remains the same and the letters are the
# same, this can be solved as a refinement of the anagram solver from
# previous chapter 12 exercises.


def read_words():
    """ read the word list in to a list """

    words = []

    f = open("./words.txt", "r+")
    for line in f.readlines():
        word = line.rstrip()

        words.append(word)

    f.close()
    return words


def map_letters(wordlist):
    """ build a dict that maps from collections of letters
    to lists of all possible anagrams """

    ana_dict = {}

    for word in wordlist:
        # put letters of word in alpha order for use as key
        sorted_word = "".join(sorted(word))
        if sorted_word in ana_dict:
            # key already exists, add to the list in the value
            ana_dict[sorted_word].append(word)
        else:
            ana_dict[sorted_word] = [word]

    return ana_dict


def find_metas(ana_list):
    """ given a list of anagrams, find all the metathesis pairs """

    meta_words = []
    word_point = 0

    # if list of possible anagrams only contains one item,
    # no anagrams or metathesis pairs are possible!
    if len(ana_list) < 2:
        return

    # start with first word in list and walk each letter
    # comparing with next word in list.  Track how many swaps
    # there are.  If it's exactly 2, this is a metathesis pair.
    while word_point < len(ana_list) - 1:
        meta_count = 0
        first = ana_list[word_point]
        second = ana_list[word_point + 1]
        for i, c in enumerate(first):
            if c != second[i]:
                meta_count += 1
        if meta_count == 2:
            meta_words.append([first, second])
        word_point += 1

    return meta_words


if __name__ == "__main__":
    print()

    my_words = read_words()

    my_ana_dict = map_letters(my_words)

    for key in my_ana_dict:
        metas = find_metas(my_ana_dict[key])
        if metas:
            for pair in metas:
                print(pair)
