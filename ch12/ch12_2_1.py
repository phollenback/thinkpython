#!/usr/bin/python3

# think python chapter 12, exercise 2 part 1

# find all the words in a word list that are anagrams


def read_words():
    """ read the word list in to a list """

    words = []

    f = open("./words.txt", "r+")
    for line in f.readlines():
        word = line.rstrip()

        words.append(word)

    f.close()
    return words


def map_letters(wordlist):
    """ build a dict that maps from collections of letters
    to lists of all possible anagrams """

    ana_dict = {}

    for word in wordlist:
        # put letters of word in alpha order for use as key
        sorted_word = "".join(sorted(word))
        if sorted_word in ana_dict:
            # key already exists, add to the list in the value
            ana_dict[sorted_word].append(word)
        else:
            ana_dict[sorted_word] = [word]

    return ana_dict


if __name__ == "__main__":
    print()

    my_words = read_words()

    my_ana_dict = map_letters(my_words)

    for key in my_ana_dict:
        if len(my_ana_dict[key]) > 1:
            print(my_ana_dict[key])
