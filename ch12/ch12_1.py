#!/usr/bin/python3

# think python chapter 12, exercise 1

# write a function called most_frequent that takes a string and
# prints the letters in decreasing order or frequency.


def invert_dict(d):
    inverse = {}
    for key in d:
        val = d[key]
        if val not in inverse:
            inverse[val] = [key]
        else:
            inverse[val].append(key)

    return inverse


def most_frequent(test_string):
    """ given a string, print the letters in decreasing order of
    frequency"""

    freq = {}

    for c in my_string:
        freq[c] = freq.get(c, 0) + 1

    counts = invert_dict(freq)

    for count in sorted(counts)[::-1]:
        print(count, "".join(counts[count]))


if __name__ == "__main__":
    print()

    my_string = "yhhuxxxxxxxxiiiiwwwoggggggggg"

    print("string:", my_string)
    print("letter frequencies:")
    most_frequent(my_string)
