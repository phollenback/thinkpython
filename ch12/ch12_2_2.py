#!/usr/bin/python3

# think python chapter 12, exercise 2 part 2

# Take problem 1 (find all the words in a word list that are
# anagrams) and modify it to print the longest anagram list
# first and then print the rest in decreasing order.


def invert_dict(a_dict):
    """ invert a dict using setdefault """

    inverse = dict()
    for key in a_dict:
        val = a_dict[key]
        inverse.setdefault(val, []).append(key)
    return inverse


def read_words():
    """ read the  word list in to a list """

    words = []

    f = open("./words.txt", "r+")
    for line in f.readlines():
        word = line.rstrip()

        words.append(word)

    f.close()
    return words


def map_letters(wordlist):
    """ build a dict that maps from collections of letters
    to lists of all possible anagrams.  Also build a parallel
    dict mapping each sorted word key to a count of how many words
    are anagrams for the sorted_word. """

    ana_dict = {}
    len_dict = {}

    for word in wordlist:
        # put letters of word in alpha order for use as key
        sorted_word = "".join(sorted(word))
        if sorted_word in ana_dict:
            # key already exists, add to the list in the value
            ana_dict[sorted_word].append(word)
            len_dict[sorted_word] += 1
        else:
            ana_dict[sorted_word] = [word]
            len_dict[sorted_word] = 1

    return (ana_dict, len_dict)


if __name__ == "__main__":
    print()

    my_words = read_words()

    (my_ana_dict, my_len_dict) = map_letters(my_words)

    # invert the length dict so we have a map of lengths to
    # sorted word keys
    invert_len_dict = invert_dict(my_len_dict)

    for key in sorted(invert_len_dict, reverse=True):
        if key > 1:
            # ignore sorted words with only one match
            # as that means there are no anagrams for
            # that word
            for ana_key in invert_len_dict[key]:
                print(my_ana_dict[ana_key])
