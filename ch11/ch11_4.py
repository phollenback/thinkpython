# think python chapter 11, exercise 4

# implement has_duplicates for a list with a dict

# has_duplicates - take a list and return True if the list contains
# any duplicate elements.


def has_duplicates(testlist):

    seen = {}

    for item in testlist:
        if item not in seen:
            seen[item] = 1
        else:
            return False

    return True


if __name__ == "__main__":

    print()

    testlists = (
        [1, 2, 3, 4, 5, 6, 7, 8],
        ["a", "b", "c", "d", "d", "e"],
        ["foo", "bar", "baz"],
        ["bar", "bar", "foo"],
    )

    for my_list in testlists:
        if has_duplicates(my_list):
            outcome = "does NOT have duplicates"
        else:
            outcome = "has duplicates"

        print(my_list, outcome)
