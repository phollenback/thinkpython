#!/usr/bin/python3

# think python chapter 11, exercise 6

# Puzzler about homophones - find a 5 letter word that is
# pronounced the same as the four letter word when you remove
# the first letter from it AND the same as the four letter word
# when you remove the second letter.

from pronounce import read_dictionary


def read_words():
    """ read the word list in to a dict

    only get words that are 4 or 5 letters long as those
    are the only words we need for this puzzler """

    four_words = {}
    five_words = {}

    f = open("./words.txt", "r+")
    for line in f.readlines():
        word = line.rstrip()
        if len(word) == 5:
            five_words[word] = five_words.get(word, 0) + 1
        if len(word) == 4:
            four_words[word] = four_words.get(word, 0) + 1

    f.close()
    return five_words, four_words


def check_homophones(five_words, four_words, pronunciations):
    """ given our list of five letter words, four letter words,
    and the pronunciation dict, create the 4-letter words based
    on each 5-letter word and check if thety are homophones"""

    results = {}

    for word in five_words:
        # first 4-letter word is the 5-letter word with first
        # character removed
        first_removed = word[1:]
        # second 4-letter word is the 5-letter word with the
        # second character removed
        second_removed = word[0] + word[2:]

        # need to check that the pronunciation dict contains
        # entries for each of the words we are checking
        if (
            word not in pronunciations
            or first_removed not in pronunciations
            or second_removed not in pronunciations
        ):
            continue

        if (
            pronunciations[word]
            == pronunciations[first_removed]
            == pronunciations[second_removed]
        ):
            results[word] = [first_removed, second_removed]

    return results


if __name__ == "__main__":
    print()

    print("finding all sets of homophones")

    (my_5_words, my_4_words) = read_words()

    pronunciations = read_dictionary()

    my_results = check_homophones(my_5_words, my_4_words, pronunciations)

    print("found following solutions:")
    for word in my_results:
        print(word, ":", my_results[word][0], my_results[word][1])
