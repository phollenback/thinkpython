#!/usr/bin/python3

# think python chapter 11, exercise 2

# implement the ackerman function using memoization

known = {}


def ack(m, n):
    """ calculate ackerman function for two numbers
    >>> ack(3,4)
    125
    """

    if (m, n) in known:
        return known[(m, n)]

    if m == 0:
        return n + 1
    if m > 0 and n == 0:
        res = ack((m - 1), 1)
        known[(m, n)] = res
        return res
    if m > 0 and n > 0:
        res = ack(m - 1, ack(m, n - 1))
        known[(m, n)] = res
        return res


if __name__ == "__main__":
    print(ack(3, 4))
