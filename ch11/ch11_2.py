#!/usr/bin/python3

# think python chapter 11, exercise 2

# implement invert_dict using setdefault


def invert_dict(a_dict):
    """ invert a dict using setdefault """

    inverse = dict()
    for key in a_dict:
        val = a_dict[key]
        inverse.setdefault(val, []).append(key)

    return inverse


if __name__ == "__main__":
    print()
    my_histo = {"a": 1, "b": 1, "o": 2}

    reversed_histo = invert_dict(my_histo)

    print(reversed_histo)
