#!/usr/bin/python3

# think python chapter 11, exercise 5

# Read a wordlist and find all words that are 'rotate pairs' -
# rotating one of them gets you the other.

# Check all 13 possible rotations foir the 26 character lowercase
# alphabet - since after 13 it just repeats.

# Use the string rotator function I wrote for chapter 8 ex 5.


def read_words():
    """ read the word list in to a dict """

    words = {}

    f = open("./words.txt", "r+")
    for line in f.readlines():
        word = line.rstrip()

        words[word] = words.get(word, 0) + 1

    f.close()
    return words


def rotate_string(my_string, my_rot_amt):
    """rotate a string by integer amount (could be a negative amount)

    >>> rotate_string("",1)
    ''
    >>> rotate_string("test", 5)
    'yjxy'
    """

    rot_string = ""

    # only worry about the a-z range:
    # a = 97, z = 122

    # if ord is btw 65 and 90, wrap in that range for uppercase
    # same btw 97 and 122 for lowercase
    for c in my_string:
        chr_ord = ord(c)
        if 97 <= chr_ord <= 122:
            # this is a lowercase letter
            start_ord = 97
        else:
            # pass through unchanged
            rot_string += c
            continue

        # shift down to start at 0
        baseline = chr_ord - start_ord

        new_ord = ((baseline + my_rot_amt) % 26) + start_ord

        rot_string += chr(new_ord)

    return rot_string


if __name__ == "__main__":
    print()

    my_words = read_words()
    total_count = 0

    print("checking for rotate pairs in a wordlist of", len(my_words), "words")

    rot_words = {}

    for i in range(1, 14):
        print("checking rotation", i)
        for word in my_words:
            rot_word = rotate_string(word, i)
            if rot_word in my_words:
                rot_words.setdefault(i, []).append([word, rot_word])

    for i in rot_words:
        print("found", len(rot_words[i]), "rotate pairs for a rotation of", i)
        total_count += len(rot_words[i])

    print("found", total_count, "total rotate pairs in wordlist")
