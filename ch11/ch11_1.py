# think python chapter 11, exercise 1

# read words.txt in to a dict and use 'in' to check whether a word is
# in the list - compare lookup speed vs using a python list

# use words.txt from previous chapters.

import timeit


def read_words():
    """ read the word list in to a dict """

    words = {}

    f = open("./words.txt", "r+")
    for line in f.readlines():
        word = line.rstrip()

        words[word] = words.get(word, 0) + 1

    f.close()
    return words


def copy_dict_list(a_dict):
    """ copy the keys of a dict in to a list """

    a_list = []

    for key in a_dict:
        a_list.append(key)

    return a_list


if __name__ == "__main__":
    print()
    my_words_dict = read_words()
    my_words_list = copy_dict_list(my_words_dict)
    my_checkwords = "cold", "schooled", "shoe", "bizapple"

    print("checking time looking up words in a dict")
    start_time = timeit.default_timer()
    for word in my_checkwords:
        if word in my_words_dict:
            print("found", word, "in wordlist")
        else:
            print(word, "not found in wordlist")
    elapsed = timeit.default_timer() - start_time
    print("elapsed time:", elapsed)

    print("checking time looking up words in a list")
    start_time = timeit.default_timer()
    for word in my_checkwords:
        if word in my_words_list:
            print("found", word, "in wordlist")
        else:
            print(word, "not found in wordlist")
    elapsed = timeit.default_timer() - start_time
    print("elapsed time:", elapsed)
