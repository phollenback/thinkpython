# think python chapter 3 exercise 2

def do_twice(f, my_arg):
    f(my_arg)
    f(my_arg)

def do_four(f, my_arg):
    do_twice(f, my_arg)
    do_twice(f, my_arg)

def print_twice(my_text):
    print(my_text)
    print(my_text)

do_four(print_twice, 'spam')
