# think python chapter 3, exercise 3

# print a 4x4 grid


def print_sep(my_cnt):
    """print the full separator line"""
    for i in range(my_cnt):
        print("+" + "-" * my_cnt, end="")
    print("+")


def print_line(my_cnt):
    """print the vertical lines only"""
    for i in range(my_cnt):
        print("|" + " " * my_cnt, end="")
    print("|")


# my_size is the length of each size, i.e. my_size 2 is a square of 2x2
my_size = 4

for i in range(my_size):
    print_sep(my_size)
    for j in range(my_size):
        print_line(my_size)

print_sep(my_size)
