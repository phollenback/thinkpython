# think python chapter 3, exercise 1

# print input string right-justified to 70 chars


def right_justify(s):
    word_len = len(s)
    start_pos = 70 - word_len

    print(" " * start_pos, s)


right_justify("monty")
